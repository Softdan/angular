import { Injectable } from "@angular/core";
import { BehaviorSubject, Subject } from "rxjs";
import { DestinoViaje } from './destino-viaje.models';
import { AppState } from '../app.module';
import { Store } from "@ngrx/store";
import { NuevoDestinoAction, ElegidoFavoritoAction } from './destinos-viajes-state.model';

@Injectable({
    providedIn: 'root'
})
export class DestinosApiClient {

    constructor(private store: Store<AppState>) {
       
    }

    add(d: DestinoViaje) {
        this.store.dispatch(new NuevoDestinoAction(d));
    }
    
    
    elegir(d: DestinoViaje) {
        this.store.dispatch(new ElegidoFavoritoAction(d));
        //this.destinos.forEach(data => data.setSelected(false));
        //d.setSelected(true);
        //this.currents.next(d);
    }

    /*
    getById(id: String): DestinoViaje {
        return this.destinos.filter((d) => { return d.id.toString() == id; })[0]
    }

    getAll(): DestinoViaje[] {
        return this.destinos;
    }

    suscribeOnchange(fn){
        this.currents.subscribe(fn);
    }
    */
}