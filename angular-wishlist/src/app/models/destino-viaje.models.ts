import { v4 as uuid } from 'uuid';

export class DestinoViaje {
		
	private selected: boolean;
	public servicios: string[];
	id = uuid();

	constructor(public nombre: string,public url: string, public votes = 0) {
		this.servicios = ['desayuno', 'almuerzo']
	}

	isSelected(): boolean {
		return this.selected;
	}

	setSelected(valor: boolean){
		this.selected = valor;
	}

	voteUp(){
		this.votes++;
	}

	voteDown(){
		this.votes--;
	}
}