import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Store } from '@ngrx/store';


import { DestinoViaje } from '../models/destino-viaje.models';
import { DestinosApiClient } from '../models/destinos-api-clients.models';
import { ElegidoFavoritoAction, NuevoDestinoAction } from '../models/destinos-viajes-state.model';
import { AppState } from '../app.module';


@Component({
	selector: 'app-lista-destinos',
	templateUrl: './lista-destinos.component.html',
	styleUrls: ['./lista-destinos.component.css']
})
export class ListaDestinosComponent implements OnInit {

	@Output() onItemAdded: EventEmitter<DestinoViaje>;
	updates: string[];
	destinos: DestinoViaje[];
	all;

	constructor(public apiClient: DestinosApiClient, private store: Store<AppState>) {
		this.onItemAdded = new EventEmitter();
		this.updates = [];

		this.store.select(state => state.destinos.favorito)
		.subscribe(data => {
		  const d = data;
		  if (d != null) {
			this.updates.push('Se eligió: ' + d.nombre);
		  }
		});
  
		store.select(state => state.destinos.items).subscribe(items => this.all = items);
	}

	ngOnInit(): void {}

	agregado(d: DestinoViaje) {
		this.apiClient.add(d);
		this.onItemAdded.emit(d);
		//this.store.dispatch(new NuevoDestinoAction(d));
	}

	elegido(e: DestinoViaje){
		this.apiClient.elegir(e);
		//this.store.dispatch(new ElegidoFavoritoAction(e));
	}

	getAll(){

	}

}